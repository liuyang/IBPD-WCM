<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>属性配置</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		body{
			overflow-y:auto;
		}
		.baseInfo{
			
			
		}
		.baseInfo tr .tit .tittext{
			font-size:10px;
			overflow:hidden;
			text-overflow:clip;
			width:80px;
		}
		.baseInfo tr .val{
			padding:2 2 2 2;
			width:120px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr .val .int {
			width:115px;
		}
		.baseInfo tr .val .int input{
			width:115px;
		}
		.baseInfo tr .val .int select{
			width:117px;
		}
		.baseInfo tr .val .int textarea{
			width:117px;
		}
		.baseInfo tr .val .assist {
			width:20px;
		}
		.baseInfo tr .val .assist input {
			width:20px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
		<div style="height:98%;overflow-y:visible;">
		<input type="hidden" id="pageTemplateId" value="${pageTemplate.id }"/>
			
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<td class="tit"><div class="tittext">模板名称</div></th>
						<td class="val">
							<span class="int">
							<c:if test="${pageTemplate.isDefault==false}">
							<input type="text" mthod="smt" name="title" value="${pageTemplate.title }"/>
							</c:if>
							<c:if test="${pageTemplate.isDefault==true}">
							<span>${pageTemplate.title }</span>
							</c:if>
							</span>
						</td>
					<tr>
					<tr>
						<td class="tit"><div class="tittext">排序</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="order" value="${pageTemplate.order }"/>
							</span>
						</td>
					<tr>
						<td class="tit"><div class="tittext">所属分组</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="group" value="${pageTemplate.group }"/>
							</span>
						</td>
					</tr>
					<tr>
						<td class="tit"><div class="tittext">使用鼠标右键</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="useMouseRightKey">
								<option value="true" <c:if test="${pageTemplate.useMouseRightKey==true }">selected</c:if>>允许</option>
								<option value="false" <c:if test="${pageTemplate.useMouseRightKey==false }">selected</c:if>>禁止</option>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<td class="tit"><div class="tittext">页面内容拷贝</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="enableCopyPageContent">
								<option value="true" <c:if test="${pageTemplate.enableCopyPageContent==true }">selected</c:if>>允许</option>
								<option value="false" <c:if test="${pageTemplate.enableCopyPageContent==false }">selected</c:if>>禁止</option>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<td class="tit"><div class="tittext">灰度页面</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="useDarkColor">
								<option value="true" <c:if test="${pageTemplate.useDarkColor==true }">selected</c:if>>是</option>
								<option value="false" <c:if test="${pageTemplate.useDarkColor==false }">selected</c:if>>否</option>
							</select>
							</span>
						</td>
					</tr>
				</table>
</div>
	
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		bindEvents();
		$("[mthod='smt']").bind("blur",function(e){
			var nodeId=$("#pageTemplateId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("value");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/NodeTemplate/saveProp.do",
				{id:nodeId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
	});   
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){parent.showFileSelecterDialog(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*上级页面调用这个函数来做到设置本页 tag值的效果   参数：name为本页tag的name值，val为具体值,具体调用效果可参阅node的index.jsp页面
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
     </script>
     
	</body>
</html>
