<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
<style media=print>   
.Noprint{display:none;}   
.PageNext{page-break-after: always;}   
table{
margin:top:20mm;
width:180mm;
}
table tr th{
	border:solid 1px black;
	background-color:#464646;
	font-weight:bold;
}
table tr td{
	border:solid 1px black;
	height:20mm;
}

</style>   
<style media="screen">
table{
	width:90%;
	margin-top:20px;
}
table tr td{
	border:solid 1px black;
	height:15px;
}

</style>
	</head>

	<body>
	<div class="Noprint">
	<OBJECT id="WebBrowser" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" height="0" width="0">   
	</OBJECT>   

		<span>选择查询日期:从 <input id="sdate" class="easyui-datebox" required="true"></input> 到 <input id="ndate" class="easyui-datebox" required="true"></input> 选择供应商:
		<select id="tenantId">
			<option value="-1">所有商户</option>
			<option value="0">平台自身商品</option>
			<c:forEach items="${tenantList }" var="tenant">
			<option value="${tenant.id}">${tenant.entFullName}</option>
			</c:forEach>
		</select>
		<input type="button" id="searchbtn" value="查询"/>
		<input type="button" id="paintbtn" onclick="document.all.WebBrowser.ExecWB(6,6)" value="打印"/>
		<br/>
		</div>
		<div>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">统计汇总:</td>
			</tr>
			<tbody  id="total">
			</tbody>
		</table>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th>商户名称</th>
				<th>订单号</th>
				<th>商品名称</th>
				<th>数量</th>
				<th>单价</th>
				<th>总价</th>
			</tr>
			<tbody id="report">
			</tbody>
		</table>
		</div>
	</body>
		<script type="text/javascript">
	$(document).ready(function(){
		$("#searchbtn").click(function(){search();});
	});
	function search(){
		$("#total").html("");
		$("#report").html("");
		var s=$("#sdate").datebox("getValue");
		var n=$("#ndate").datebox("getValue");
		var t=$("#tenantId").val();
		$.post(
				basePath+"/Manage/Sell/getSellReport.do",
				{"startDate":s,"endDate":n,"tenantId":t},
				function(result){
					if(result.indexOf("msg:")>=0){
						var _s=eval("("+result+")");
						alert(_s.msg);
					}else{
						var _s=eval("("+result+")");
						var total=_s.total;
						var report=_s.rows;
						var totalhtml="";
						for(i=0;i<total.length;i++){
							totalhtml+="<tr><td>"+total[i].k+"</td><td>"+total[i].v+"</td></tr>";
						}
						$(totalhtml).appendTo("#total");
						var reporthtml="";
						for(i=0;i<report.length;i++){
							reporthtml+="<tr><td>"+report[i].tenantName+"</td><td>"+report[i].orderId+"</td><td>"+report[i].productName+"</td>";
							//reporthtml+="<td>"+report[i].tenantName+"</td><td>"+report[i].orderId+"</td><td>"+report[i].productName+"</td>";
							reporthtml+="<td>"+report[i].number+"</td><td>"+report[i].price_d+"</td><td>"+report[i].total_d+"</td>";
						//	reporthtml+="<td>"+report[i].startDate+"</td><td>"+report[i].endDate+"</td></tr>";
							reporthtml+="</tr>";
						}
						$(reporthtml).appendTo("#report");
					}
				}
			);
	};
	</script>
</html>
