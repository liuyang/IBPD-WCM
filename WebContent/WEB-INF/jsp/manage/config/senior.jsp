<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
        <table>
				<tr>
					<td class="tit">
						<span>站点开启规则</span>
					</td>
					<td class="val">
						<table>
						<tr>
							<td colspan="2">
						<select id="timmingOpen_select" style="width:100%;" onchange="swap(this)">
							<option id="1">启用</option>
							<option id="0" selected>禁用</option>
						</select>							
							</td>
						</tr>
						<tr class="open_tr">
							<td width="40%">
								<input type="radio" id="singDate_radio" name="timmingType_date"/>具体日期
								</td>
								<td width="60%">
								<input type="text" id="open_singDate_val" style="width:100%"/>
								</td>
							
						</tr>
						<tr class="open_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每个月
							</td>
							<td>
							第<select id="open_month_days_select" style="width:59px"></select>日,时间:<input type="text" style="width:76px" id="open_month_days_val"/>
							</td>
						</tr>
						<tr class="open_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每周
							
							</td>
							<td>
							第<select id="open_week_day_select" style="width:59px"></select>日,时间:<input type="text" style="width:76px" id="open_week_days_val"/>
							</td>
						</tr>
						<tr class="open_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每天
							</td>
							<td>
							<input type="text" style="width:100%" id="open_evaryday_val"/>
							</td>
						</tr>
							</table>
					</td>
					<td class="rem">
						<span>${cRemMap.siteOpenRule}</span>
					</td>
				</tr>
				<tr>
					<td class="tit">
						<span>站点关闭规则</span>
					</td>
					<td class="val">
						<table>
						<tr>
							<td colspan="2">
						<select id="timmingClose_select" style="width:100%;" onchange="swap(this)">
							<option id="1">启用</option>
							<option id="0" selected>禁用</option>
						</select>							
							</td>
						</tr>
						<tr class="close_tr">
							<td width="40%">
								<input type="radio" id="singDate_radio" name="timmingType_date"/>具体日期
								</td>
								<td width="60%">
								<input type="text" id="close_singDate_val" style="width:100%"/>
								</td>
							
						</tr>
						<tr class="close_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每个月
							</td>
							<td>
							第<select id="close_month_days_select" style="width:59px"></select>日,时间:<input type="text" style="width:76px" id="close_month_days_val"/>
							</td>
						</tr>
						<tr class="close_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每周
							
							</td>
							<td>
							第<select style="width:59px" id="close_week_day_select"></select>日,时间:<input type="text" style="width:76px" id="close_week_days_val"/>
							</td>
						</tr>
						<tr class="close_tr">
							<td>
							<input type="radio" id="date" name="timmingType_date"/>每天
							</td>
							<td>
							<input type="text" style="width:100%" id="close_evaryday_val"/>
							</td>
						</tr>
							</table>
					</td>
					<td class="rem">
						<span>${cRemMap.siteOpenRule}</span>
					</td>
				</tr>
			
        
        </table>
