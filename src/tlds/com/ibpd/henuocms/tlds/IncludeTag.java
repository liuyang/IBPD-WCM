package com.ibpd.henuocms.tlds;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;

import org.h2.util.StringUtils;
import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.TxtUtil;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.web.WebSite;

public class IncludeTag extends SimpleBaseTag {
	
	private String nodeId="OWNER";

	private NodeExtEntity node=null;
	@Override
	public void doTag() throws JspException, IOException {
		_init();
		if(node==null){
            return ;
        }
		String uriPath=getNodeUriDir(node,this.getRequest());
		uriPath=getRequest().getRealPath(uriPath);
		String txt=TxtUtil.readTxtFile(uriPath);
	    getJspContext().getOut().write(txt);
	    if(getJspBody()!=null)
	        getJspBody().invoke(null);
	}
	private String getNodeUriDir(NodeEntity node,HttpServletRequest req){
		if(node==null)
			return "";
		ISubSiteService siteServ=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
		SubSiteEntity site=siteServ.getEntityById(node.getSubSiteId());
		String idPath=node.getNodeIdPath();
		String[] ids=idPath.split(",");
		Map<Long,String> dirMap=new LinkedHashMap<Long,String>();
		//先把要取的目录都取出来
		INodeAttrService attrService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
		for(String id:ids){
			if(StringUtil.isEmpty(id))
				continue;
			if(StringUtils.isNumber(id)){
				Long nodeId=Long.parseLong(id);
				NodeAttrEntity attr=attrService.getNodeAttr(nodeId);
				if(attr!=null){
					dirMap.put(nodeId, attr.getDirectory());//理论上这里取出来的目录顺序是正确的，这里之所以要把目录放入跟ID对照的map中，是为了以后扩展方便
				}
			}
			
		}  
		String dir="";  
		if(site!=null){ 
			dir="sites"+File.separator+site.getDirectory()+File.separator;
		}
		for(Long key:dirMap.keySet()){
			dir+=dirMap.get(key)+File.separator; 
		}
		dir=dir+"index.html";
		return dir;
	}
	protected void _init() {
		if(!StringUtils.isNullOrEmpty(nodeId)){
			if(nodeId.toUpperCase().trim().equals("OWNER")){
				nodeId=getRequest().getParameter("nodeId");
			}else if(StringUtils.isNumber(nodeId)){
				 
			}else{
				nodeId="-1";
			}
			Long currentNodeId=Long.valueOf(nodeId);
			if(!currentNodeId.equals(-1L)){
				INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				NodeEntity n=nodeServ.getEntityById(currentNodeId);
				NodeExtEntity ext=new NodeExtEntity();
				ext.setNode(n);
				INodeAttrService attrServ=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
				NodeAttrEntity attr=attrServ.getNodeAttr(currentNodeId);
				ext.setAttr(attr);
				node=ext;
			}
		}else{
			node=null;
		}
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeId() {
		return nodeId;
	}
}