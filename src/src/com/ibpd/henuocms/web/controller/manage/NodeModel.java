package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.ModelEntity;
import com.ibpd.henuocms.entity.ValidateEntity;
import com.ibpd.henuocms.service.model.IModelService;
import com.ibpd.henuocms.service.model.ModelServiceImpl;
import com.ibpd.henuocms.service.validate.IValidateService;
import com.ibpd.henuocms.service.validate.ValidateServiceImpl;

@Controller
public class NodeModel extends BaseController {
	private IModelService modelService=null;
	@RequestMapping("manage/model/index.do")
	public String index(HttpServletResponse response) throws IOException{
		return "manage/model/index";
	}	
	@RequestMapping("manage/model/doAdd.do")
	public String doAdd(String modelName,String intro,Integer order) throws IOException{
		modelService=(IModelService) ((modelService==null)?ServiceProxyFactory.getServiceProxy(ModelServiceImpl.class):modelService);
		modelService.setDao(new DaoImpl());
		ModelEntity model=new ModelEntity();
		model.setCreateTime(new Date());
		model.setIntro(intro);
		model.setModelName(modelName);
		model.setOrder(order);
		modelService.saveEntity(model);
		return "manage/model/index";
	}
	@RequestMapping("manage/model/doEdit.do")
	public String doEdit(Long id,String modelName,String intro,Integer order) throws IOException{
		modelService=(IModelService) ((modelService==null)?ServiceProxyFactory.getServiceProxy(ModelServiceImpl.class):modelService);
		modelService.setDao(new DaoImpl());
		ModelEntity model=modelService.getEntityById(id);
		if(model!=null){
			model.setIntro(intro);
			model.setModelName(modelName);
			model.setOrder(order);
			modelService.saveEntity(model);	
		}else{
			//还没考虑好怎么捕获异常
		}
		return "manage/model/index";
	}
	@RequestMapping("manage/model/doDel.do")
	public String doDel(String ids) throws IOException{
		modelService=(IModelService) ((modelService==null)?ServiceProxyFactory.getServiceProxy(ModelServiceImpl.class):modelService);
		modelService.setDao(new DaoImpl());
		modelService.batchDel(ids);
		return "manage/model/index";
	}
	@RequestMapping("manage/model/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryData){
		modelService=(IModelService) ((modelService==null)?ServiceProxyFactory.getServiceProxy(ModelServiceImpl.class):modelService);
		modelService.setDao(new DaoImpl());
		super.getList(req, modelService, resp, order, page, rows, sort, queryData);
	}
}
