package com.ibpd.henuocms.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
/**
 * 管理权限filter
 * @author mg by qq:349070443
 *
 */
@Component
public class ManagePermissionCheckFilter implements Filter {
	Logger log=Logger.getLogger(ManagePermissionCheckFilter.class);
     public void destroy() {
    }
 
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req=(HttpServletRequest) request;
    	HttpServletResponse resp=(HttpServletResponse) response;
    	String url=req.getServletPath();
       	if(url.contains("Manage/login.do")){
    		chain.doFilter(request, response);
    		return;
    	}
       	if(url.contains("Manage/User/doLogin.do")){
    		chain.doFilter(request, response);
    		return;
    	} 
    	if(url.contains("Manage/")){
    		if(SysUtil.getCurrentLoginedUserName(req.getSession())==null){
    			log.error("=|=|=|=|=|=|执行登录");
    			resp.sendRedirect(req.getContextPath()+"/Manage/login.do");
    		} 
    	}
        chain.doFilter(request, response);
    } 
    public void init(FilterConfig filterConfig) throws ServletException {
   
    } 
}