package com.ibpd.henuocms.entity.ext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
/**
 * 节点栏目 扩展类
 * @author mg by qq:349070443
 *
 */
public class NodeExtEntity extends NodeEntity{

	private NodeAttrEntity attr;
	private NodeEntity node;

	private Integer orderByNavigator=0;

	public void setAttr(NodeAttrEntity attr) {
		this.attr = attr;
		if(attr!=null){
			this.orderByNavigator=attr.getOrderByNavigator();
		}
	}

	public NodeAttrEntity getAttr() {
		return attr;
	}

	public void setNode(NodeEntity node) {
		this.node = node;
		if(node!=null){
		try {
			setValue(node);
			IbpdLogger.getLogger(this.getClass()).info(this.toString());
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		}
	}

	private void setValue(NodeEntity node) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method[] mts=node.getClass().getMethods();
		for(Method mt:mts){
			String  mtName=mt.getName();
			if(mtName.substring(0,3).equals("get")){
				String tName="set"+mtName.substring(3);
				if(existMethod(tName)){
					if(tName.equals("setId")){
//						IbpdLogger.getLogger(this.getClass()).info();
					}
					Method mthod=this.getClass().getMethod(tName, mt.getReturnType());
					mthod.invoke(this, mt.invoke(node, null));
				}
			}
		}
	}
	private Boolean existMethod(String mtName){
		Method[] mt=this.getClass().getMethods();
		for(Method m:mt){
			if(m.getName().equals(mtName)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "NodeExtEntity [orderByNavigator=" + orderByNavigator
				+ ", getBanner()=" + getBanner() + ", getBgMusic()="
				+ getBgMusic() + ", getChildCount()=" + getChildCount()
				+ ", getCopyright()=" + getCopyright()
				+ ", getCountentCount()=" + getCountentCount()
				+ ", getCreateDate()=" + getCreateDate() + ", getCreateUser()="
				+ getCreateUser() + ", getCustom1()=" + getCustom1()
				+ ", getCustom10()=" + getCustom10() + ", getCustom2()="
				+ getCustom2() + ", getCustom3()=" + getCustom3()
				+ ", getCustom4()=" + getCustom4() + ", getCustom5()="
				+ getCustom5() + ", getCustom6()=" + getCustom6()
				+ ", getCustom7()=" + getCustom7() + ", getCustom8()="
				+ getCustom8() + ", getCustom9()=" + getCustom9()
				+ ", getDepth()=" + getDepth() + ", getDescription()="
				+ getDescription() + ", getGroup()=" + getGroup()
				+ ", getIcon()=" + getIcon() + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getKeywords()="
				+ getKeywords() + ", getLeaf()=" + getLeaf()
				+ ", getLinkUrl()=" + getLinkUrl() + ", getLogo()=" + getLogo()
				+ ", getManageUser()=" + getManageUser() + ", getMap()="
				+ getMap() + ", getNodeIdPath()=" + getNodeIdPath()
				+ ", getNodeType()=" + getNodeType() + ", getOrder()="
				+ getOrder() + ", getParentId()=" + getParentId()
				+ ", getState()=" + getState() + ", getSubContentCountUnit()="
				+ getSubContentCountUnit() + ", getSubSiteId()="
				+ getSubSiteId() + ", getText()=" + getText() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}

	public NodeEntity getNode() {
		return node;
	}

	public void setOrderByNavigator(Integer orderByNavigator) {
		this.orderByNavigator = orderByNavigator;
	}

	public Integer getOrderByNavigator() {
		return orderByNavigator;
	}
}
