package com.ibpd.henuocms.test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import freemarker.template.Template;

/**
 * @author Michael
 *
 */
public class TableGenerator {

    /** 
     * tableVo
     */
    private FormTable tableVo;

    /**
     * 脚本文件
     */
    private String scriptFileName = "d:/test/table.sql";

    /**
     * 构造函数
     * @param tableVo
     */
    public TableGenerator(FormTable tableVo) {
        this.tableVo = tableVo;
    }

    /**
     * 构造函数
     * @param tableVo
     * @param scriptFileName
     */
    public TableGenerator(FormTable tableVo, String scriptFileName) {
        this.tableVo = tableVo;
        if (null != scriptFileName && !"".equals(scriptFileName)) {
            this.scriptFileName = scriptFileName;
        }
    }

    /**
     *
     */
    public void generatorTable() {
        if (tableVo.getFormAttributeList().isEmpty()) {
            System.out.println(" column attr list size==0 ");
            return;
        }

        Template tl;
        try {
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("entity", tableVo);

            tl = getTemplateConfig("/com/ibpd/henuocms/test").getTemplate(
                    "template.hb.ftl");
            Writer out = new StringWriter();
            tl.process(paramMap, out);
            String hbxml = out.toString();
            System.out.println(hbxml);
            Configuration hbcfg = this.getHibernateCfg(hbxml);

            // Properties pp = CommonUtil
            // .getPropertiesByResource(Constant.PPFILENAME);
            // DataSource ds = BasicDataSourceFactory.createDataSource(pp);

            createDbTableByCfg(hbcfg);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取freemarker的cfg
     * @param resource
     * @return Configuration
     */
    protected freemarker.template.Configuration getTemplateConfig(
            String resource) {

        freemarker.template.Configuration cfg = new freemarker.template.Configuration();
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(this.getClass(), resource);
        return cfg;
    }

    /**
     * 处理hibernate的配置文件
     * @param resource
     */
    protected Configuration getHibernateCfg(String hbxml) {
        org.hibernate.cfg.Configuration hbcfg = new org.hibernate.cfg.Configuration();
        hbcfg.configure("/hibernateConfig.xml");
        Properties extraProp = new Properties();
        extraProp.put("hibernate.hbm2ddl.auto", "update");
        hbcfg.addProperties(extraProp);
        hbcfg.addXML(hbxml);
        return hbcfg;
    }

    /**
     * 根据hibernate cfg配置文件动态建表
     * @param hbcfg
     */
    public void createDbTableByCfg(Configuration hbcfg) {
        SchemaExport schemaExport;
        try {
            schemaExport = new SchemaExport(hbcfg);
            // 设置脚本文件
            schemaExport.setOutputFile(scriptFileName);
            schemaExport.create(true, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据配置文件、Connection 来动态建表
     * @param conf
     * @param ds
     */
    public void createDbTableByConn(Configuration conf, DataSource ds) {
        SchemaExport schemaExport;
        try {

            schemaExport = new SchemaExport(conf, ds.getConnection());
            schemaExport.setOutputFile(scriptFileName);
            schemaExport.create(true, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}