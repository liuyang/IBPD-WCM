package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_UserTenant")
public class UserTenantEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_userId",nullable=true)
	private Long userId;
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
}
